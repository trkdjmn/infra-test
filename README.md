## TiiFa Infra

`Project: TiiFa`
`IaC: Terraform`

これはTiiFaのインフラをコード化した物

## How to Deploy Infra

Terraform Cloudを利用しStaging/Productionに反映が行われる
修正された環境のみPlanが実行される

https://app.terraform.io/app/ks-rogers/workspaces

1. Create branch
2. Change Enviroment file(./terraform/services/[project-name]/[env])
3. push
4. merge request to master

```
例：
    terraform/services/yamasa-global-id/staging配下のファイルを修正する
    TerraformCloud上のyamasa-global-id-stagingでPlanが発火
```

### Codecommitに関して

Terraform上でcodecommitの定義を行い、Gitlabとのミラーリング設定を行っている  
ただし、Git認証情報に関しては手動にて作成するものとする  

`aws iam create-service-specific-credential --user-name global-id-codecommit-user --service-name codecommit.amazonaws.com`

実行後発行された認証情報をGitlabに設定する ※基本的には一度設定すれば終わり

## Dir Architecture

```
]# tree
.
tqq README.md
mqq terraform
    tqq modules
    x?? tqq alb
    x?? x?? tqq main.tf
    x?? x?? mqq variable.tf
    x?? tqq alb-redash
    x?? x?? tqq main.tf
    x?? x?? mqq variable.tf
    x?? tqq cloudwatch
    x?? x?? tqq main.tf
    x?? x?? mqq variable.tf
    x?? tqq codebuild
    x?? x?? tqq main.tf
    x?? x?? tqq policies
    x?? x?? x?? tqq codebuild_assume_role.json.tpl
    x?? x?? x?? mqq codebuild_policy.json.tpl
    x?? x?? mqq variable.tf
    x?? tqq codecommit
    x?? x?? tqq main.tf
    x?? x?? tqq policies
    x?? x?? x?? mqq codecommit_user_policy.json.tpl
    x?? x?? mqq variable.tf
    x?? tqq codedeploy
    x?? x?? tqq main.tf
    x?? x?? tqq policies
    x?? x?? x?? mqq codedeploy_assume_role.json.tpl
    x?? x?? mqq variable.tf
    x?? tqq codepipeline
    x?? x?? tqq main.tf
    x?? x?? tqq policies
    x?? x?? x?? tqq codepipeline_assume_role.json.tpl
    x?? x?? x?? mqq codepipeline_policy.json.tpl
    x?? x?? mqq variable.tf
    x?? tqq ec2
    x?? x?? tqq main.tf
    x?? x?? mqq variable.tf
    x?? tqq ecr
    x?? x?? tqq main.tf
    x?? x?? tqq policies
    x?? x?? x?? mqq ecr_policy.json.tpl
    x?? x?? mqq variable.tf
    x?? tqq ecs-ec2
    x?? x?? tqq main.tf
    x?? x?? tqq policies
    x?? x?? x?? tqq ec2_assume_role.json.tpl
    x?? x?? x?? mqq ecs_task_assume_role.json.tpl
    x?? x?? mqq variable.tf
    x?? tqq ecs-fargate
    x?? x?? tqq main.tf
    x?? x?? tqq policies
    x?? x?? x?? mqq ecs_task_assume_role.json.tpl
    x?? x?? tqq task-definitions
    x?? x?? x?? mqq service.json
    x?? x?? mqq variable.tf
    x?? tqq rds
    x?? x?? tqq main.tf
    x?? x?? mqq variable.tf
    x?? tqq s3
    x?? x?? tqq main.tf
    x?? x?? mqq variable.tf
    x?? tqq ssm_parameter
    x?? x?? tqq main.tf
    x?? x?? mqq variable.tf
    x?? mqq vpc
    x??     tqq main.tf
    x??     mqq variable.tf
    mqq services
        tqq common
        x?? tqq backend.tf
        x?? tqq main.tf
        x?? tqq policies
        x?? x?? mqq s3_logs_policy.json.tpl
        x?? mqq variable.tf
        tqq redash
        x?? tqq backend.tf
        x?? tqq main.tf
        x?? mqq variable.tf
        mqq web
            tqq common
            x?? tqq backend.tf
            x?? tqq main.tf
            x?? mqq variable.tf
            mqq staging
                tqq backend.tf
                tqq main.tf
                tqq templates
                x?? mqq taskdef_json.tpl
                mqq variable.tf

33 directories, 56 files
```

# Do Not Delete Workspace
