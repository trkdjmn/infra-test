module "tiifa-redash" {
  ### Module Path
  source              = "../../modules/ec2"
  ami_id              = "ami-060741a96307668be"
  instance_type       = "t2.small"
  key_pair_name       = "tiifa-staging"
  volume_type         = "gp2"
  volume_size         = "8"
  instance_name       = "tiifa-redash"
  subnet_id           = "subnet-03170189a0c82534e"
  security_group_id_1 = "sg-08ad39c36badb3c45"
  security_group_id_2 = "sg-03419de5800d48195"
  eip_name            = "tiifa-redash"
}
module "tiifa-redash-alb" {
  source            = "../../modules/alb"
  alb_name          = "tiifa-redash-alb"
  tg_name           = "tiifa-redash-alb-tg"
  certificate_arn   = "arn:aws:acm:ap-northeast-1:887891188376:certificate/f9d1fddb-edf2-4daa-9de2-e837e30edfcd"
  health_path       = "/login"
  vpcid             = "vpc-02f0b6a6c14c39811"
  security_group_id = "sg-03419de5800d48195"
  subnet_id_1       = "subnet-03170189a0c82534e"
  subnet_id_2       = "subnet-0368667b35f9bb2f7"
  instance_ip       = "10.0.0.187"
}
