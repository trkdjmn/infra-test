module "codecommit_tiifa-web" {
  source = "../../modules/codecommit"

  repo_name       = "tiifa-web"
  desc_codecommit = "tiifa gitlab"
  user_name       = "tiifa-codecommit-user"
  policy_name     = "tiifa-codecommit-policy"
}
