[
        {
            "name": "${container_name_1}",
            "image": "httpd",
            "essential": true,
            "memory": 200,
            "portMappings": [
                {
                    "hostPort": 0,
                    "containerPort": 80,
                    "protocol": "tcp"
                }
            ],
            "logConfiguration": {
                "logDriver": "awslogs",
                "options": {
                    "awslogs-group": "/ecs/${log_group_name}",
                    "awslogs-region": "ap-northeast-1",
                    "awslogs-stream-prefix": "${container_name_2}"
                }
            }
      },
        {
            "name": "${container_name_2}",
            "image": "httpd",
            "essential": true,
            "memory": 200,
            "logConfiguration": {
                "logDriver": "awslogs",
                "options": {
                    "awslogs-group": "/ecs/${log_group_name}",
                    "awslogs-region": "ap-northeast-1",
                    "awslogs-stream-prefix": "${container_name_2}"
                }
            }
      }
]
