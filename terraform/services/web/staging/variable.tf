## IAM Setting
variable "aws_access_key" {}
variable "aws_secret_key" {}

## Project Name
variable "project" {
  type    = string
  default = "tiifa"
}
variable "system" {
  type    = string
  default = "web"
}
variable "environment" {
  type    = string
  default = "stg"
}

## VPC Parameter
variable "vpcid" {}
variable "accountid" {}
variable "subnet_id_1a" {}
variable "subnet_id_1c" {}

variable "db_password" {
  type    = string
  default = "password"
}

## ACM Parameter
# variable alb_acm_arn {}

## Codecommit Parameter
# variable aws_lb_target_group_arn {}

## Paramter List
