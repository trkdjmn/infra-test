locals {
  repositories = [
    "nginx",
    "php"
  ]

  containers = [
    "tifa-nginx-cli",
    "tifa-php-cli"
  ]
}

module "ecr_private_repository" {
  ### Module Path
  source = "../../../modules/ecr"
  for_each = toset(local.repositories)

  # resource aws_ecr_repository
  repo_name = "${var.project}-${var.system}-${var.environment}-${each.value}"

  # resource aws_ecr_repository_policy
  ## none

  # output ecr_uri

}

data "template_file" "taskdef_json" {
  # temporary taskdef
  template = file("${path.module}/templates/taskdef_json.tpl")

  vars = {
    container_name_1         = local.containers[0]
    container_name_2         = local.containers[1]
    log_group_name = "${var.project}-${var.system}-${var.environment}"
  }
}

module "ecs_cluster_set" {
  ### Module Path
  source = "../../../modules/ecs-ec2"

  # resource aws_cloudwatch_log_group
  log_group_name = "${var.project}-${var.system}-${var.environment}"

  # resource aws_iam_role
  # resource aws_iam_instance_profile
  # resource aws_iam_role_policy_attachment
  ecs_task_role_name = "${var.project}-ecs-task-role"
  ec2_role_name      = "${var.project}-ec2-role"

  # resource aws_ecs_cluster
  cluster_name = "${var.project}-${var.system}-${var.environment}"

  # resource aws_security_group
  container_sg_name = "${var.project}-sg-application-${var.environment}"
  container_sg_desc = "for application"
  sg_alb_id         = module.alb_for_ecs.sg_alb_id
  vpcid             = var.vpcid

  # resource aws_launch_configuration
  ec2_keyname = "${var.project}-${var.environment}"

  # resource aws_autoscaling_group
  # var.cluster_name
  subnet_id_1 = var.subnet_id_1a
  subnet_id_2 = var.subnet_id_1c

  # resource aws_ecs_capacity_provider
  # var.cluster_name

  # resource aws_ecs_service
  ecs_service_name        = "${var.project}-${var.system}-${var.environment}"
  aws_lb_target_group_arn = module.alb_for_ecs.aws_lb_target_group-1_arn
  alb_container_name      = local.containers[0]

  # resource aws_ecs_task_definition
  taskdef_name = "${var.project}-${var.system}-${var.environment}"
  taskdef_json = data.template_file.taskdef_json.rendered
  accountid    = var.accountid
  # var.ecs_task_role_name

  # output sg_application_id
}

module "alb_for_ecs" {
  ### Module Path
  source = "../../../modules/alb"

  # resource aws_security_group
  vpcid       = var.vpcid
  alb_sg_name = "${var.project}-sg-alb-${var.environment}"

  # resource aws_alb
  alb_name      = "${var.project}-${var.system}-${var.environment}-alb"
  subnet_id_1   = var.subnet_id_1a
  subnet_id_2   = var.subnet_id_1c
  alblog_bukect = "${var.project}-${var.environment}"

  # resource aws_alb_listener
  #  certificate_arn = var.alb_acm_arn

  # resource aws_alb_target_group
  # var.vpcid
  tg_name     = "${var.project}-${var.system}-${var.environment}"
  health_path = "/login"

  # output sg_alb_id
  # output aws_alb_listener_arn
  # output aws_lb_target_group-1_arn
  # output aws_lb_target_group-2_arn

}

module "ssmparameter_parameter_store" {
  ### Module Path
  source = "../../../modules/ssm_parameter"

  # resource aws_ssm_parameter
  service_path = "${var.project}-${var.system}-${var.environment}"

  ### Paramter List
  hostname = module.rds_sqlserver.rds_hostname
  username = "${var.project}_${var.environment}"
  password = var.db_password

}

module "codebuild_project" {
  ### Module Path
  source = "../../../modules/codebuild"

  # resource aws_iam_role
  # resource aws_iam_role_policy
  # resource aws_iam_role_policy_attachment
  accountid             = var.accountid
  codebuild_role_name   = "${var.project}-${var.system}-${var.environment}-codebuild-role"
  codebuild_policy_name = "${var.project}-${var.system}-${var.environment}-codebuild-policy"
  codecommit_repo_name  = "${var.project}-${var.system}"
  artifact_bucket_name  = "${var.project}-codepipeline-artifact"
  # var.codebuild_name

  # resource aws_codebuild_project
  codebuild_name = "${var.project}-${var.system}-${var.environment}-build"
  codebuild_desc = "web staging build"
  buildspec_path = "codepipeline/buildspec.yml"
  codecommit_url = "https://git-codecommit.ap-northeast-1.amazonaws.com/v1/repos/${var.project}-${var.system}"
  source_version = "refs/heads/feature/codebuild-check"

  ### Codebuild env
  #repository_uri  = module.ecr_private_repository.ecr_uri
  env             = "${var.project}-${var.system}-${var.environment}"
  cluster_name    = "${var.project}-${var.system}-${var.environment}"
  #task_run_subnet = var.subnet_id_1a

  # output codebuild_project_name
}

module "codedeploy_project" {
  ### Module Path
  source = "../../../modules/codedeploy"

  # resource aws_iam_role
  # resource aws_iam_role_policy_attachment
  codedeploy_role_name = "${var.project}-${var.system}-${var.environment}-codedeploy-role"

  # resource aws_codedeploy_app
  codedeploy_app_name = "${var.project}-${var.system}-${var.environment}"

  # resource aws_codedeploy_deployment_group
  deployment_group_name = "${var.project}-${var.system}-${var.environment}"
  cluster_name          = "${var.project}-${var.system}-${var.environment}"
  service_name          = "${var.project}-${var.system}-${var.environment}"
  ecs_listener_arn      = module.alb_for_ecs.aws_alb_listener_arn
  blue_target_name      = module.alb_for_ecs.aws_lb_target_group-1_arn
  green_target_name     = module.alb_for_ecs.aws_lb_target_group-2_arn

  depends_on = [
    module.ecs_cluster_set
  ]

}

module "codepipeline_pipeline" {
  ### Module Path
  source = "../../../modules/codepipeline"

  # resource aws_iam_role
  # resource aws_iam_role_policy
  codepipeline_role_name   = "${var.project}-${var.system}-${var.environment}-codepipeline-role"
  codepipeline_policy_name = "${var.project}-${var.system}-${var.environment}-codepipeline-policy"

  # resource aws_s3_bucket
  codepipeline_artifact_bucket_name = "${var.project}-${var.system}-${var.environment}-codepipeline-artifact"

  # resource aws_codepipeline
  codepipeline_name = "${var.project}-${var.system}-${var.environment}"

  ## Source Stage
  repository_name = "${var.project}-${var.system}"
  branch_name     = "develop"

  ## Build Stage
  codebuild_project_name = module.codebuild_project.codebuild_project_name

  ## Deploy Stage
  ApplicationName     = "${var.project}-${var.system}-${var.environment}"
  DeploymentGroupName = "${var.project}-${var.system}-${var.environment}"
  taskdef_path        = "codepipeline/taskdef.json"
  appspec_path        = "codepipeline/appspec.yml"

}

module "rds_sqlserver" {
  ### Module Path
  source = "../../../modules/rds"

  subnet_group_name = "${var.project}-${var.environment}"
  subnet = [
    var.subnet_id_1a,
    var.subnet_id_1c
  ]

  availability_zone = "ap-northeast-1a"
  db_name           = "${var.project}-${var.environment}"
  db_username       = "${var.project}_${var.environment}"
  db_password       = var.db_password
  option_group_name = "default:sqlserver-ex-15-00"
  # character_set_name = 
  parameter_group_name = "${var.project}-${var.environment}"
  vpcid                = var.vpcid
  rds_sg_name          = "${var.project}-${var.environment}"
  sg_application_id    = module.ecs_cluster_set.sg_application_id

  # output rds_hostname
}
