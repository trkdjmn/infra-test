module "tiifa-vpc" {
  ### Module Path
  source = "../../modules/vpc"

  ### CIDR Block Setting
  cidr_vpc = "10.0.0.0/16"

  ### Subnet Setting
  cidr_public = [
    "10.0.0.0/24",
    "10.0.1.0/24"
  ]

  cidr_private = [
    "10.0.100.0/24",
    "10.0.101.0/24"
  ]

  subnet_name = "tiifa-subnet"

  ### RouteTable Setting
  route_table_name = "tiifa-rtb"

  ### IGW Setting
  igw_name = "tiifa-igw"

  ### SecurityGroup Setting
  sg_name = "tiifa-sg"

  ### Peering Setting
  enable_peering = false
  #peering_cidr_block = var.peering_cidr_block
  #peering_id = var.peering_id 

  ### Tag
  tag = "tiifa"
}

module "s3_tiifa-production" {
  ### Module Path
  source = "../../modules/s3"

  bucket_name         = "tiifa-prd"
  env                 = "prd"
  atttach_policy_bool = 0
  bucket_policy       = "none"
}

module "s3_tiifa-staging" {
  ### Module Path
  source = "../../modules/s3"

  bucket_name         = "tiifa-stg"
  env                 = "stg"
  atttach_policy_bool = 0
  bucket_policy       = "none"
}

data "template_file" "s3_logs_policy_json" {
  template = file("${path.module}/policies/s3_logs_policy.json.tpl")

  vars = {
    accountid   = var.accountid
    bucket_name = "tiifa-logs"
  }
}

module "s3_tiifa-log" {
  ### Module Path
  source = "../../modules/s3"

  bucket_name         = "tiifa-logs"
  env                 = "common"
  atttach_policy_bool = 1
  bucket_policy       = data.template_file.s3_logs_policy_json.rendered

}


