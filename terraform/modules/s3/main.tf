resource "aws_s3_bucket" "s3_bucket" {
  bucket = var.bucket_name
  acl    = "private"

  tags = {
    Name        = var.bucket_name
    Environment = var.env
  }
}

resource "aws_s3_bucket_policy" "policy_attach" {
  count = var.atttach_policy_bool

  bucket = aws_s3_bucket.s3_bucket.id
  policy = var.bucket_policy
}
