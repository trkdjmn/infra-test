output "sg_alb_id" {
  value = aws_security_group.sg_alb.id
}

output "aws_lb_target_group-1_arn" {
  value = aws_alb_target_group.tg-1.arn
}

output "aws_lb_target_group-2_arn" {
  value = aws_alb_target_group.tg-2.arn
}

output "aws_alb_listener_arn" {
  value = aws_alb_listener.alb-listener-http.arn
}

resource "aws_security_group" "sg_alb" {
  name        = var.alb_sg_name
  description = var.alb_sg_name
  vpc_id      = var.vpcid

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = var.alb_sg_name
  }
}

resource "aws_alb" "alb" {
  name                       = var.alb_name
  internal                   = false
  security_groups            = [aws_security_group.sg_alb.id]
  subnets                    = [var.subnet_id_1, var.subnet_id_2]
  enable_deletion_protection = false

  #  access_logs {
  #    bucket  = var.alblog_bukect
  #    prefix  = var.alb_name
  #    enabled = true
  #  }

  tags = {
    Name        = var.alb_name
  }
}

resource "aws_alb_listener" "alb-listener-http" {
  load_balancer_arn = aws_alb.alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.tg-1.arn
    type             = "forward"
  }

  lifecycle {
    ignore_changes = [
      default_action
    ]
  }
}

#resource aws_alb_listener alb-listener-https {
#  load_balancer_arn = aws_alb.alb.arn
#  port              = "443"
#  protocol          = "HTTPS"
#  ssl_policy        = "ELBSecurityPolicy-2015-05"
#  certificate_arn   = var.certificate_arn
#
#  default_action {
#    target_group_arn = aws_alb_target_group.tg-1.arn
#    type             = "forward"
#  }

#  lifecycle {
#    ignore_changes = ["default_action"]
#  }
#}

resource "aws_alb_target_group" "tg-1" {
  name        = "${var.tg_name}-1"
  port        = 80
  target_type = "instance"
  # target_type          = "ip" fargate
  protocol             = "HTTP"
  vpc_id               = var.vpcid
  deregistration_delay = 10

  health_check {
    interval            = 300
    path                = var.health_path
    port                = "traffic-port"
    protocol            = "HTTP"
    timeout             = 5
    unhealthy_threshold = 2
    matcher             = 200
  }

  stickiness {
    enabled             = true
    cookie_duration     = 86400
    type                = "lb_cookie"
  }
}

resource "aws_alb_target_group" "tg-2" {
  name        = "${var.tg_name}-2"
  port        = 80
  target_type = "instance"
  # target_type          = "ip" fargate
  protocol             = "HTTP"
  vpc_id               = var.vpcid
  deregistration_delay = 10

  health_check {
    interval            = 300
    path                = var.health_path
    port                = "traffic-port"
    protocol            = "HTTP"
    timeout             = 5
    unhealthy_threshold = 2
    matcher             = 200
  }

  stickiness {
    enabled             = true
    cookie_duration     = 86400
    type                = "lb_cookie"
  }
}
