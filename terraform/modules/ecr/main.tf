output "ecr_uri" {
  value = aws_ecr_repository.ecr_repository.repository_url
}

resource "aws_ecr_repository" "ecr_repository" {
  name = var.repo_name
}

data "template_file" "ecr-policy-json" {
  template = file("${path.module}/policies/ecr_policy.json.tpl")
}

resource "aws_ecr_repository_policy" "ecr-policy" {
  repository = aws_ecr_repository.ecr_repository.name
  policy     = data.template_file.ecr-policy-json.rendered
}
