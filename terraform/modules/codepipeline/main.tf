data "template_file" "codepipeline_assume_role_json" {
  template = file("${path.module}/policies/codepipeline_assume_role.json.tpl")
}

data "template_file" "codepipeline_policy_json" {
  template = file("${path.module}/policies/codepipeline_policy.json.tpl")
}

resource "aws_iam_role" "codepipeline_role" {
  name               = var.codepipeline_role_name
  assume_role_policy = data.template_file.codepipeline_assume_role_json.rendered
}

resource "aws_iam_role_policy" "codepipeline_policy" {
  name = var.codepipeline_policy_name
  role = aws_iam_role.codepipeline_role.id

  policy = data.template_file.codepipeline_policy_json.rendered
}

resource "aws_s3_bucket" "s3_codepipeline_artifact" {
  bucket = var.codepipeline_artifact_bucket_name
  acl    = "private"
}

data "aws_kms_alias" "artifact_kms_key" {
  name = "alias/aws/s3"
}

resource "aws_codepipeline" "codepipeline" {
  name     = var.codepipeline_name
  role_arn = aws_iam_role.codepipeline_role.arn

  artifact_store {
    location = aws_s3_bucket.s3_codepipeline_artifact.bucket
    type     = "S3"

    encryption_key {
      id   = data.aws_kms_alias.artifact_kms_key.arn
      type = "KMS"
    }
  }

  stage {
    name = "Source"

    action {
      run_order        = 1
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeCommit"
      version          = "1"
      output_artifacts = ["SourceArtifact"]

      configuration = {
        RepositoryName       = var.repository_name
        BranchName           = var.branch_name
        OutputArtifactFormat = "CODE_ZIP"
        PollForSourceChanges = "true"
      }
    }
  }

  stage {
    name = "Build"

    action {
      run_order        = 1
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      version          = "1"
      input_artifacts  = ["SourceArtifact"]
      output_artifacts = ["BuildArtifact"]

      configuration = {
        ProjectName = var.codebuild_project_name
      }
    }
  }

  stage {
    name = "Deploy"
    action {

      run_order       = 1
      name            = "Deploy"
      category        = "Deploy"
      owner           = "AWS"
      provider        = "CodeDeployToECS"
      input_artifacts = ["BuildArtifact"]
      version         = "1"
      namespace       = "DeployVariables"

      configuration = {
        ApplicationName                = var.ApplicationName
        DeploymentGroupName            = var.DeploymentGroupName
        TaskDefinitionTemplateArtifact = "BuildArtifact"
        TaskDefinitionTemplatePath     = var.taskdef_path
        AppSpecTemplateArtifact        = "BuildArtifact"
        AppSpecTemplatePath            = var.appspec_path
        Image1ArtifactName             = "BuildArtifact"
        Image1ContainerName            = "IMAGE_NAME"

      }
    }
  }
}
