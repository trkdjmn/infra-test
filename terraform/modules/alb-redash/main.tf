resource "aws_alb" "public-alb" {
  name                       = var.alb_name
  internal                   = false
  security_groups            = [var.security_group_id]
  subnets                    = [var.subnet_id_1, var.subnet_id_2]
  enable_deletion_protection = false

  tags = {
    Environment = var.alb_name
  }
}

resource "aws_alb_listener" "alb-listener-http" {
  load_balancer_arn = aws_alb.public-alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_alb_listener" "alb-listener-https" {
  load_balancer_arn = aws_alb.public-alb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = var.certificate_arn

  default_action {
    target_group_arn = aws_alb_target_group.public-alb-tg.arn
    type             = "forward"
  }
}

resource "aws_alb_target_group" "public-alb-tg" {
  name                 = var.tg_name
  port                 = 80
  protocol             = "HTTP"
  target_type          = "ip"
  vpc_id               = var.vpcid
  deregistration_delay = 10

  health_check {
    interval            = 300
    path                = var.health_path
    port                = "traffic-port"
    protocol            = "HTTP"
    timeout             = 5
    unhealthy_threshold = 2
    matcher             = 200
  }
}

resource "aws_alb_target_group_attachment" "tg-attach-ip" {
  target_group_arn = aws_alb_target_group.public-alb-tg.arn
  target_id        = var.instance_ip
  port             = 80
}
