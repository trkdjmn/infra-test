resource "aws_instance" "amibase_instance" {
  ami           = var.ami_id
  instance_type = var.instance_type
  key_name      = var.key_pair_name
  subnet_id     = var.subnet_id
  vpc_security_group_ids = [
    var.security_group_id_1,
    var.security_group_id_2
  ]
  root_block_device {
    volume_type = var.volume_type
    volume_size = var.volume_size
  }
  tags = {
    Name = var.instance_name
  }
  lifecycle {
    ignore_changes = [
      "associate_public_ip_address", "public_dns", "public_ip"
    ]
  }
}

resource "aws_eip" "aws_eip" {
  instance = aws_instance.amibase_instance.id
  vpc      = true
  tags = {
    Name = var.eip_name
  }
}
