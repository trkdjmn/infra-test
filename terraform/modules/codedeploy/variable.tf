variable "codedeploy_app_name" {}
variable "codedeploy_role_name" {}
variable "deployment_group_name" {}
variable "cluster_name" {}
variable "service_name" {}
variable "ecs_listener_arn" {}
variable "blue_target_name" {}
variable "green_target_name" {}
