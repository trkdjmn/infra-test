output "rds_hostname" {
  value = aws_db_instance.db_instance.address
}

resource "aws_db_subnet_group" "subnet_group_private" {
  name        = var.subnet_group_name
  description = var.subnet_group_name
  subnet_ids  = var.subnet
}

resource "aws_db_parameter_group" "db_parameter_group" {
  name   = var.parameter_group_name
  description = var.parameter_group_name
  family = "sqlserver-ex-15.0"
}

resource "aws_db_instance" "db_instance" {
  allocated_storage           = 20
  allow_major_version_upgrade = false
  auto_minor_version_upgrade  = false
  apply_immediately           = true
  backup_retention_period     = 7
  copy_tags_to_snapshot       = true
  deletion_protection         = true

  availability_zone    = var.availability_zone
  db_subnet_group_name = aws_db_subnet_group.subnet_group_private.name
  # character_set_name = var.character_set_name
  vpc_security_group_ids          = [aws_security_group.sg_rds.id]
  #enabled_cloudwatch_logs_exports = ["agent", "error"]

  engine              = "sqlserver-ex"
  engine_version      = "15.00.4073.23.v1"
  license_model       = "license-included"

  instance_class       = "db.t3.small"
  identifier           = var.db_name
  username             = var.db_username
  password             = var.db_password
  option_group_name    = var.option_group_name
  parameter_group_name = aws_db_parameter_group.db_parameter_group.name
  skip_final_snapshot  = true

  lifecycle {
    ignore_changes = [
      allocated_storage,
      availability_zone
    ]
  }
}

resource "aws_security_group" "sg_rds" {
  name        = var.rds_sg_name
  description = var.rds_sg_name
  vpc_id      = var.vpcid

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    security_groups = ["${var.sg_application_id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.rds_sg_name
  }
}
