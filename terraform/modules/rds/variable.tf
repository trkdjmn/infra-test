variable "subnet" {}
variable "subnet_group_name" {}
variable "availability_zone" {}
variable "db_name" {}
variable "db_username" {}
variable "db_password" {}
variable "option_group_name" {}
# variable character_set_name {}
variable "parameter_group_name" {}
variable "vpcid" {}
variable "rds_sg_name" {}
variable "sg_application_id" {}
