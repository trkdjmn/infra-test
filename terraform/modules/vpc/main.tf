output "project_vpc_id" {
  value = aws_vpc.project_vpc.id
}

output "project_subnet_public_id_1" {
  value = element(aws_subnet.project_subnet_public.*.id, 1)
}

output "project_subnet_public_id_2" {
  value = element(aws_subnet.project_subnet_public.*.id, 2)
}

output "project_subnet_private_id_1" {
  value = element(aws_subnet.project_subnet_private.*.id, 1)
}

output "project_subnet_private_id_2" {
  value = element(aws_subnet.project_subnet_private.*.id, 2)
}

resource "aws_vpc" "project_vpc" {
  cidr_block           = var.cidr_vpc
  enable_dns_hostnames = true
  tags = {
    Name = var.tag
  }
}

resource "aws_internet_gateway" "project_igw" {
  vpc_id = aws_vpc.project_vpc.id
  tags = {
    Name = var.igw_name
  }
}

resource "aws_subnet" "project_subnet_public" {
  count             = length(var.cidr_public)
  vpc_id            = aws_vpc.project_vpc.id
  cidr_block        = element(var.cidr_public, count.index)
  availability_zone = var.availability_zones[count.index]
  tags = {
    Name = "${var.subnet_name}-public-${count.index + 1}"
  }
}

resource "aws_route_table" "project_public_table" {
  vpc_id = aws_vpc.project_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.project_igw.id
  }

  tags = {
    Name = "${var.route_table_name}-public"
  }
}

resource "aws_route_table_association" "project_tableassociation_public" {
  count          = length(var.cidr_public)
  subnet_id      = element(aws_subnet.project_subnet_public.*.id, count.index)
  route_table_id = aws_route_table.project_public_table.id
}

resource "aws_subnet" "project_subnet_private" {
  count             = length(var.cidr_private)
  vpc_id            = aws_vpc.project_vpc.id
  cidr_block        = element(var.cidr_private, count.index)
  availability_zone = var.availability_zones[count.index]
  tags = {
    Name = "${var.subnet_name}-private-${count.index + 1}"
  }
}

resource "aws_route_table" "project_private_table" {
  vpc_id = aws_vpc.project_vpc.id

  tags = {
    Name = "${var.route_table_name}-private"
  }
}

resource "aws_route_table_association" "project_tableassociation_private" {
  count          = length(var.cidr_private)
  subnet_id      = element(aws_subnet.project_subnet_private.*.id, count.index)
  route_table_id = aws_route_table.project_private_table.id
}

resource "aws_security_group" "project_sg_alb" {
  name        = "${var.sg_name}-alb"
  description = "for http and https"
  vpc_id      = aws_vpc.project_vpc.id
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.sg_name}-alb"
  }
}

resource "aws_security_group" "project_sg_application" {
  name        = "${var.sg_name}-application"
  description = "for http and https"
  vpc_id      = aws_vpc.project_vpc.id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.sg_name}-application"
  }
}

resource "aws_security_group" "project_sg_gateway" {
  name        = "${var.sg_name}-gateway"
  description = "for ssh"
  vpc_id      = aws_vpc.project_vpc.id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.sg_name}-gateway"
  }
}

#resource aws_route project_peering_route {
#  count = var.enable_peering?1:0
#
#  route_table_id              = aws_route_table.project_public_table.id
#  destination_cidr_block      = var.peering_cidr_block
#  vpc_peering_connection_id   = var.peering_id
#}

