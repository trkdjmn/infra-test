variable "cidr_vpc" {}
variable "cidr_public" {}
variable "cidr_private" {}
variable "route_table_name" {}
variable "tag" {}
variable "subnet_name" {}
variable "igw_name" {}
variable "sg_name" {}
#variable peering_id {}
#variable peering_cidr_block {}

variable "enable_peering" {
  description = "If set to true, enable peering route"
  type        = bool
}

variable "availability_zones" {
  type    = list(string)
  default = ["ap-northeast-1a", "ap-northeast-1c"]
}
