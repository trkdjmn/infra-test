output "codebuild_project_name" {
  value = aws_codebuild_project.codebuild_project.name
}

data "template_file" "codebuild_assume_role_json" {
  template = file("${path.module}/policies/codebuild_assume_role.json.tpl")
}

data "template_file" "codebuild_policy_json" {
  template = file("${path.module}/policies/codebuild_policy.json.tpl")

  vars = {
    account_id           = var.accountid
    region               = "ap-northeast-1"
    codebuild_name       = var.codebuild_name
    codecommit_name      = var.codecommit_repo_name
    artifact_bucket_name = var.artifact_bucket_name
  }
}

resource "aws_iam_role" "codebuild-role" {
  name               = var.codebuild_role_name
  assume_role_policy = data.template_file.codebuild_assume_role_json.rendered
}

resource "aws_iam_role_policy" "codebuild-policy" {
  name = var.codebuild_policy_name
  role = aws_iam_role.codebuild-role.id

  policy = data.template_file.codebuild_policy_json.rendered
}

resource "aws_iam_role_policy_attachment" "run-task-policy" {
  role       = aws_iam_role.codebuild-role.id
  count      = length(var.iam_policy_arn)
  policy_arn = var.iam_policy_arn[count.index]
}

resource "aws_codebuild_project" "codebuild_project" {
  name          = var.codebuild_name
  description   = var.codebuild_desc
  build_timeout = "100"
  service_role  = aws_iam_role.codebuild-role.arn

  artifacts {
    type = "NO_ARTIFACTS"
  }

  cache {
    type  = "LOCAL"
    modes = ["LOCAL_DOCKER_LAYER_CACHE", "LOCAL_SOURCE_CACHE"]
  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/standard:5.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = true

    #environment_variable {
    #  name  = "REPOSITORY_URI"
    #  value = var.repository_uri
    #}

    environment_variable {
      name  = "ENV"
      value = var.env
    }

    environment_variable {
      name  = "CLUSTER_NAME"
      value = var.cluster_name
    }

    #environment_variable {
    #  name  = "TASK_RUN_SUBNET"
    #  value = var.task_run_subnet
    #}
  }

  source {
    type            = "CODECOMMIT"
    location        = var.codecommit_url
    git_clone_depth = 1
    buildspec       = var.buildspec_path
  }

  source_version = var.source_version
}
