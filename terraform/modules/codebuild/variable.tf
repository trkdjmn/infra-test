variable "codebuild_name" {}
variable "codebuild_desc" {}

variable "codecommit_repo_name" {}
variable "source_version" {}

variable "codebuild_role_name" {}
variable "codebuild_policy_name" {}

variable "buildspec_path" {}

variable "artifact_bucket_name" {}
variable "codecommit_url" {}
#variable "repository_uri" {}
variable "cluster_name" {}
#variable "task_run_subnet" {}
variable "env" {}
variable "accountid" {}

variable "iam_policy_arn" {
  type = list(string)
  default = [
    "arn:aws:iam::aws:policy/AmazonECS_FullAccess",
    "arn:aws:iam::aws:policy/AmazonS3FullAccess"
  ]
}
