output "sg_application_id" {
  value = aws_security_group.sg_application.id
}

data "template_file" "ecs_task_assume_role_json" {
  template = file("${path.module}/policies/ecs_task_assume_role.json.tpl")
}

resource "aws_cloudwatch_log_group" "cloudwatch-log" {
  name = "/ecs/${var.log_group_name}"
}

resource "aws_iam_instance_profile" "ecs_task_role_profile" {
  name = var.ecs_task_role_name
  role = aws_iam_role.ecs_task_role.name
}

resource "aws_iam_role" "ecs_task_role" {
  name               = var.ecs_task_role_name
  assume_role_policy = data.template_file.ecs_task_assume_role_json.rendered
}

resource "aws_iam_role_policy_attachment" "ecs_task_policy" {
  role       = aws_iam_role.ecs_task_role.id
  count      = length(var.iam_policy_arn)
  policy_arn = var.iam_policy_arn[count.index]
}

data "template_file" "ec2_assume_role_json" {
  template = file("${path.module}/policies/ec2_assume_role.json.tpl")
}

resource "aws_iam_instance_profile" "ec2_role_profile" {
  name = var.ec2_role_name
  role = aws_iam_role.ec2_role.name
}

resource "aws_iam_role" "ec2_role" {
  name               = var.ec2_role_name
  assume_role_policy = data.template_file.ec2_assume_role_json.rendered
}

resource "aws_ecs_cluster" "ecs_cluster" {
  name               = var.cluster_name
  capacity_providers = [aws_ecs_capacity_provider.ecs_capacity_provider.name]

  default_capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.ecs_capacity_provider.name
    weight            = 1
    base              = 2
  }
}

resource "aws_iam_role_policy_attachment" "ec2_policy" {
  role       = aws_iam_role.ec2_role.id
  count      = length(var.iam_policy_arn)
  policy_arn = var.iam_policy_arn[count.index]
}

resource "aws_iam_instance_profile" "ec2_container_service" {
  name = "es2_container_service"
  role = aws_iam_role.ec2_role.name
}

data "aws_ami" "ami_ecs" {
  filter {
    name   = "name"
    values = ["amzn2-ami-ecs-hvm-2.0.20210902-x86_64-ebs"]
  }
  owners = ["amazon"]
}

resource "aws_security_group" "sg_application" {
  name        = var.container_sg_name
  description = var.container_sg_desc
  vpc_id      = var.vpcid

  ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = ["${var.sg_alb_id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.container_sg_name
  }
}

resource "aws_launch_configuration" "ecs_launchconfig" {
  name_prefix   = "ecs-launch-tf-"
  image_id      = data.aws_ami.ami_ecs.id
  instance_type = "t3.micro"
  key_name      = var.ec2_keyname

  security_groups      = [aws_security_group.sg_application.id]
  enable_monitoring    = true
  iam_instance_profile = aws_iam_instance_profile.ec2_container_service.name

  user_data = <<EOF
#!/bin/bash
echo ECS_CLUSTER=${var.cluster_name} >> /etc/ecs/ecs.config;
EOF

  associate_public_ip_address = true

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "ecs_autoscale" {
  name             = var.cluster_name
  min_size         = 2
  max_size         = 2
  desired_capacity = 2

  launch_configuration = aws_launch_configuration.ecs_launchconfig.name
  vpc_zone_identifier  = [var.subnet_id_1, var.subnet_id_2]

  protect_from_scale_in = true

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [load_balancers, target_group_arns]
  }

  tags = [
    {
      key                 = "AmazonECSManaged"
      propagate_at_launch = true
      value               = ""
    }
  ]
}

resource "aws_ecs_capacity_provider" "ecs_capacity_provider" {
  name = "${var.cluster_name}-capacity_provider"

  auto_scaling_group_provider {
    auto_scaling_group_arn         = aws_autoscaling_group.ecs_autoscale.arn
    managed_termination_protection = "ENABLED"

    managed_scaling {
      status          = "ENABLED"
      target_capacity = 100
    }
  }
}

resource "aws_ecs_service" "ecs_service" {
  name = var.ecs_service_name

  capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.ecs_capacity_provider.name
    weight            = 1
    base              = 2
  }

  cluster                           = aws_ecs_cluster.ecs_cluster.id
  task_definition                   = aws_ecs_task_definition.taskdef_service.arn
  health_check_grace_period_seconds = 60

  desired_count                      = 2
  deployment_minimum_healthy_percent = 100
  deployment_maximum_percent         = 200
  deployment_controller {
    type = "CODE_DEPLOY"
  }

  load_balancer {
    target_group_arn = var.aws_lb_target_group_arn
    container_name   = var.alb_container_name
    container_port   = "80"
  }
  lifecycle {
    ignore_changes = [
      desired_count
#      task_definition
    ]
  }
}

resource "aws_ecs_task_definition" "taskdef_service" {
  family                = var.taskdef_name
  container_definitions = var.taskdef_json

  requires_compatibilities = ["EC2"]
  task_role_arn            = "arn:aws:iam::${var.accountid}:role/${var.ecs_task_role_name}"
  execution_role_arn       = "arn:aws:iam::${var.accountid}:role/ecsTaskExecutionRole"
  network_mode             = "bridge"

  lifecycle {
    ignore_changes = [
      container_definitions
    ]
  }
}

