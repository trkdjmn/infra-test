variable "cluster_name" {}
variable "container_sg_name" {}
variable "container_sg_desc" {}

variable "vpcid" {}
variable "subnet_id_1" {}
variable "subnet_id_2" {}
variable "ec2_keyname" {}

variable "ecs_service_name" {}
variable "alb_container_name" {}
variable "aws_lb_target_group_arn" {}
variable "sg_alb_id" {}
variable "log_group_name" {}

variable "taskdef_name" {}
variable "taskdef_json" {}
variable "accountid" {}

variable "ecs_task_role_name" {}
variable "ec2_role_name" {}
variable "iam_policy_arn" {
  type = list(string)
  default = [
    "arn:aws:iam::aws:policy/AmazonS3FullAccess",
    "arn:aws:iam::aws:policy/CloudWatchFullAccess",
    "arn:aws:iam::aws:policy/AmazonSSMFullAccess",
    "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
  ]
}
