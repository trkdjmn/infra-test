{
    "Version": "2012-10-17",
    "Statement" : [
        {
            "Effect" : "Allow",
            "Action" : [
                "codecommit:GitPull",
                "codecommit:GitPush"
            ],
            "Resource" : "${codecommit_arn}"
        }
    ]
}
