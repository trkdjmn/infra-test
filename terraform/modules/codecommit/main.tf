output "codecommit_arn" {
  value = aws_codecommit_repository.codecommit_repository.arn
}

data "template_file" "codecommit_policy_json" {
  template = file("${path.module}/policies/codecommit_user_policy.json.tpl")

  vars = {
    codecommit_arn = aws_codecommit_repository.codecommit_repository.arn
  }
}

resource "aws_codecommit_repository" "codecommit_repository" {
  repository_name = var.repo_name
  description     = var.desc_codecommit
}

resource "aws_iam_user" "codecommit_iam" {
  name = var.user_name
}

resource "aws_iam_user_policy" "codecommit_user_policy" {
  name = var.policy_name
  user = aws_iam_user.codecommit_iam.name

  policy = data.template_file.codecommit_policy_json.rendered
}
