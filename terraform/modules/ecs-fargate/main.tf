output "ecs_cluster_name" {
  value = aws_ecs_cluster.ecs_cluster.name
}

data "template_file" "ecs_task_assume_role_json" {
  template = file("${path.module}/policies/ecs_task_assume_role.json.tpl")
}

resource "aws_iam_instance_profile" "ecs_task_role_profile" {
  name = var.ecs_task_role_name
  role = aws_iam_role.ecs_task_role.name
}

resource "aws_iam_role" "ecs_task_role" {
  name               = var.ecs_task_role_name
  assume_role_policy = data.template_file.ecs_task_assume_role_json.rendered
}

resource "aws_iam_role_policy_attachment" "ecs_task_policy" {
  role       = aws_iam_role.ecs_task_role.id
  count      = length(var.iam_policy_arn)
  policy_arn = var.iam_policy_arn[count.index]
}

resource "aws_ecs_cluster" "ecs_cluster" {
  name = var.cluster_name

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_security_group" "sg_application" {
  name        = var.container_sg_name
  description = var.container_sg_desc
  vpc_id      = var.vpcid

  ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = ["${var.sg_alb_id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.container_sg_name
  }
}

resource "aws_ecs_service" "ecs_service" {
  name = var.ecs_service_name

  # depends_on = ["aws_lb_listener_rule.main"]
  cluster         = aws_ecs_cluster.ecs_cluster.name
  desired_count   = 2
  task_definition = aws_ecs_task_definition.taskdef_service.arn

  network_configuration {
    subnets          = [var.subnet_id_1, var.subnet_id_2]
    security_groups  = [aws_security_group.sg_application.id]
    assign_public_ip = true
  }

  capacity_provider_strategy {
    capacity_provider = "FARGATE"
    base              = 0
    weight            = 0
  }

  capacity_provider_strategy {
    capacity_provider = "FARGATE_SPOT"
    base              = 0
    weight            = 1
  }

  load_balancer {
    target_group_arn = var.aws_lb_target_group_arn
    container_name   = var.system
    container_port   = "80"
  }

  scheduling_strategy                = "REPLICA"
  deployment_minimum_healthy_percent = 100
  deployment_maximum_percent         = 200
  deployment_controller {
    type = "CODE_DEPLOY"
  }

  lifecycle {
    ignore_changes = [
      desired_count,
      task_definition,
      capacity_provider_strategy
    ]
  }
}

resource "aws_ecs_task_definition" "taskdef_service" {
  family                = var.taskdef_name
  container_definitions = <<JSON
 [
        {
            "name": "api",
            "image": "httpd",
            "essential": true,
            "memory": 200,
            "portMappings": [
                {
                    "hostPort": 80,
                    "containerPort": 80,
                    "protocol": "tcp"
                }
            ]
      }
]
  JSON
  #file("${path.module}/task-definitions/service.json")

  requires_compatibilities = ["FARGATE"]
  cpu                      = "512"
  memory                   = "1024"
  task_role_arn            = "arn:aws:iam::${var.accountid}:role/${var.ecs_task_role_name}"
  execution_role_arn       = "arn:aws:iam::${var.accountid}:role/ecsTaskExecutionRole"
  network_mode             = "awsvpc"

  lifecycle {
    ignore_changes = [
      container_definitions
    ]
  }
}
