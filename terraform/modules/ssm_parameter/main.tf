locals {
  parameter_list = {
    "hostname" = var.hostname
    "username" = var.username
    "password" = var.password
  }
}

data "aws_kms_alias" "parameter_kmskey" {
  name = "alias/aws/ssm"
}

resource "aws_ssm_parameter" "ssm_parameter" {
  for_each = local.parameter_list

  name   = "/${var.service_path}/${each.key}"
  value  = each.value
  type   = "SecureString"
  key_id = data.aws_kms_alias.parameter_kmskey.arn
}
